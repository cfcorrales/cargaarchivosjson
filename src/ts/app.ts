///<reference path="../../tools/typings/tsd.d.ts" />
///<reference path="../../tools/typings/typescriptApp.d.ts" />

((): void => {

    angular.module(AppSettings.nameApplication, ['ui.router', 'uiGmapgoogle-maps', AppSettings.dependenciesModule])

        .config(['$stateProvider', '$urlRouterProvider', 'uiGmapGoogleMapApiProvider', ($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider): void => {
            uiGmapGoogleMapApiProvider.configure({});
            $urlRouterProvider.otherwise('/start');
            $stateProvider
                .state('start', {
                    cache: false,
                    url: '/start',
                    templateUrl: 'templates/cargaOficinasJSON.html',
                    controller: 'astapp.controllers.CargaJSONController'
                });
        }])
    ;
})();