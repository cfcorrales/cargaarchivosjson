/**
 * Created by jchenao on 02/06/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module astappControllers {
    'use strict';

    interface IWindowServiceExtended extends ng.IWindowService {
        _: any;
    }

    interface IScopeExtend extends ng.IScope {
        listaResultado:Array<IResultado>;
        cb_insertOfficesJSON : () => void;
    }

    export interface IResultado {
        Procesado: string;
    }

    class CargaJSONController {
        static $inject = ['$window', '$http', '$scope'];
        constructor(private $window: IWindowServiceExtended , private $http: ng.IHttpService, private $scope:IScopeExtend) {
            //Controller Body
            var self = this;
            $scope.listaResultado = [];
            $scope.cb_insertOfficesJSON = () => {
                self.insertOfficesJSON();
            };
        }

        private insertOfficesJSON(): void {
            var self = this;
            self.$http.get('../json/Geolocations.json').
            success(function (data: Array<any>) {
                    self.$window._.forEach(data, function(DataOrigen: any){
                        self.$http.post('http://172.18.198.27/ApiServer_Prod/odata/Geolocations', DataOrigen).
                        success(function(data: any) {
                                self.$scope.listaResultado.push({Procesado: JSON.stringify(data)});
                        }).
                        error(function(data, status){
                                self.$scope.listaResultado.push({Procesado: JSON.stringify(data)});
                        });
                });
            }).error(function (data, status) {
                console.log('Error Creando');
            });
    }
    }
    angular.module('astapp.controllers')
        .controller('astapp.controllers.CargaJSONController', CargaJSONController);
}
