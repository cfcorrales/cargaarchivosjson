/**
 * Created by ralopezn on 31/03/2015.
 */

module Entities {

    export enum StatusOptions {
        Close = 0,
        Open = 1,
        Pending = 2
    }

    export interface AsyncArray<T> extends Array<T> {
        $promise: ng.IPromise<Array<T>>;
    }
    export interface ODataArray {
        '@odata.context': string;
        'value': Array<any>;
    }
    export interface ODataObject {
        '@odata.context': string;
    }
    interface Dictionary<T> {
        [index: string]: T;
    }

    export interface AsyncDictionary<T> {
        $promise: ng.IPromise<Dictionary<T>>;
    }

    interface ModelBase <T> {
        $promise?: ng.IPromise<T>;
    }

    export interface BaseEntity<TEntity> extends ModelBase<TEntity> {
        CreatedAt: Date;
        CreatedBy: string;
        ModifiedAt: Date;
        ModifiedBy: string;
        Status: boolean;
    }

    export interface Client extends BaseEntity<Client> {
        Id: string;
        Name: string;
        Logo: string;
        Offices:Array<Entities.Office>;
    }

    export interface Location {
        latitude: string;
        longitude: string
    }

    export interface Geolocation extends BaseEntity<Geolocation>{
        Id: string;
        AssetId: string;
        Latitude: string;
        Longitude: string
    }

    export interface Resource extends BaseEntity<Resource> {
        Id: string;
        UniqueIdentifier: string;
        GeolocationId: string;
        Thumbs: string;
        GPSSampleTime: number;
        Issues:Array<Entities.Issue>;
    }

    export interface Issue extends BaseEntity<Issue> {
        Id: string;
        IssueAssignatedDate: Date;
        IssueExpirationDate: Date;
        ResourceId: string;
        Icon: string;
        OfficeId: string;
        Resource: Entities.Resource;
        Office: Entities.Office;
        Ticket: Entities.Ticket;
    }

    export interface Ticket extends BaseEntity<Ticket>{
        IssueId: string;
        CountryId: string;
        StateId: string;
        CityId: string;
        LocalityId: string;
        Ref: string;
        TicketStatus: string;
        Description: string;
        Address: string;
        Phone: string;
        Email: string;
        Issue: Entities.Issue;
        Locality: Entities.Locality;
        City: Entities.City;
        State: Entities.State;
        Country: Entities.Country;
    }

    export interface DashBoardAlert {
        id: number;
        count: number;
        referIssuesId: string;
    }

    export enum Priority {
        Ninguna = 0, //green
        Baja = 1,    //orange
        Media = 2,   //purple
        Alta = 3,   //yellow
        Critica = 4  //red
    }

    export interface IEstadoIssue {
        prioridad: Priority;
        mensaje: string;
        color: string;
    }

    export interface IEstadoForIssues {
        statusCritica: number;
        statusAlta: number;
        statusMedia: number;
        statusBaja: number;
        statusNinguna: number;
    }

    export interface Office extends BaseEntity<Office> {
        Id: string;
        ClientId: string;
        GeolocationId: string;
        Address: string;
        ZoneId: string;
        Zone: Entities.Zone;
        Client: Entities.Client;
        Issues:Array<Entities.Issue>;
    }

    export interface MultiEntity extends BaseEntity<MultiEntity> {
        Id: string;
        Value: Number;
        Name: string;
        Code: string;
    }

    export interface Country extends MultiEntity {}
    export interface State extends MultiEntity {}
    export interface City extends MultiEntity {}
    export interface Locality extends MultiEntity {}
    export interface Zone extends MultiEntity {}

    export interface Alert extends BaseEntity<Alert> {
        MinutesToTriggerAlert: number;
        Message: string;
        Id: string;
        Color: string;
        Priority: number;
    }

    export interface UserAuthenticatedInfo extends ModelBase<UserAuthenticatedInfo> {
        UID: string;
        UserName: string;
        Email: string;
        Name: string;
        LastName: string;
    }

    export interface ResourceExtended extends Resource {
        UserInfo: Entities.UserAuthenticatedInfo;
    }
}